from python:3-alpine

env user admin
env password pass
env email veye@mail.ru

workdir /app
run mkdir /app/db
copy requirements.txt /app
run pip install -r requirements.txt
copy . .

expose 8000
volume ["/app/db"]

cmd sh init.sh && python manage.py runserver 0.0.0.0:8000
